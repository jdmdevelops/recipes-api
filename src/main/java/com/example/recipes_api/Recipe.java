package com.example.recipes_api;

import java.util.ArrayList;
import java.util.Collections;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Recipe {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	private String name;
	private String image;
	private ArrayList<String> ingredients = new ArrayList<String>();
	private ArrayList<String> steps = new ArrayList<String>();

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public ArrayList<String> getSteps() {
		return steps;
	}

	public void setSteps(String steps[]) {
		this.steps = new ArrayList<String>();
		Collections.addAll(this.steps, steps);
	}

	public ArrayList<String> getIngredients() {
		return ingredients;
	}

	public void setIngredients(String ingredients[]) {
		this.ingredients = new ArrayList<String>();
		Collections.addAll(this.ingredients, ingredients);
	}
}

package com.example.recipes_api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(path = "/recipe")
public class RecipeController {
	@Autowired
	private RecipeRepository recipeRepository;

	@PostMapping()
	public @ResponseBody String addNewRecipe(@RequestBody Recipe newRecipe) {
		recipeRepository.save(newRecipe);
		return "Saved";
	}

	@GetMapping()
	public @ResponseBody Iterable<Recipe> getAllRecipes() {
		return recipeRepository.findAll();
	}

	@PutMapping()
	public @ResponseBody String updateRecipe(@RequestBody Recipe updatedRecipe) {
		recipeRepository.save(updatedRecipe);
		return "Updated " + updatedRecipe.getId();
	}

	@DeleteMapping()
	public @ResponseBody String deleteRecipe(@RequestBody Recipe recipe) {
		recipeRepository.delete(recipe);
		return "deleted: " + recipe.getId();
	}

}
